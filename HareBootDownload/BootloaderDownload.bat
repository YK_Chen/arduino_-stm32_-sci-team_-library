@echo off
chcp 65001

for %%a in (c d e f g h i j k l m n o p q r s t u v w x y z) do (
    for /f %%h in ('fsutil fsinfo drivetype %%a:^|findstr "Removable.* STLINKV2-1"') do (
	set DriveU=%%h
	echo STLINKV2-1 is %%h .
	xcopy "%cd%\mylib\*.*" "%%h\" /y /e
    )
)


if "%DriveU%" == "" ( echo.&echo  %%h  
echo Cannot find STLINKV2-1... )


choice /t 2 /d y /n >nul

exit
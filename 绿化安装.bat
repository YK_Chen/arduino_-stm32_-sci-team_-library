@ECHO OFF&PUSHD %~DP0 &TITLE 绿化卸载(cyk修改)
mode con cols=45 lines=33
color 2F
:: 权限
>NUL 2>&1 REG.exe query "HKU\S-1-5-19" || (
    ECHO SET UAC = CreateObject^("Shell.Application"^) > "%TEMP%\Getadmin.vbs"
    ECHO UAC.ShellExecute "%~f0", "%1", "", "runas", 1 >> "%TEMP%\Getadmin.vbs"
    "%TEMP%\Getadmin.vbs"
    DEL /f /q "%TEMP%\Getadmin.vbs" 2>NUL
    Exit /b
)
:Menu
Cls
@ echo.
@ echo.  菜单选项：
@ echo.
@ echo.   1.绿化[必须]
@ echo.
@ echo.   2.一键引导程序下载
@ echo.
@ echo.   3.退出
@ echo. 

set /p xj= 输入数字按回车：
if /i "%xj%"=="1" Goto Install
if /i "%xj%"=="2" Goto HareBootDownload
if /i "%xj%"=="3" Goto Exit
@ echo.
echo 　　    选择无效...请重新输入...
ping -n 2 127.1>nul
goto menu
:Install
:: 安装前结束相关进程
@ echo.
echo  正在绿化中，请稍等
::安装驱动
cd /d %~dp0\drivers\win
start cmd /k call install_drivers.bat
ping -n 2 127.1>nul
start cmd /k call install_STM_COM_drivers.bat
ping -n 2 127.1>nul
cd /d %~dp0\drivers\CH341SER-Driver
start "" "SETUP.EXE"
ver | find "5.1." > NUL &&  goto WinXP
ver | find "6.1." > NUL &&  goto Win7or8
ver | find "6.2." > NUL &&  goto Win7or8
ver | find "10.0." > NUL &&  goto Win10
:WinXP
cd /d %~dp0\drivers\CP210x\WinXP
start "" "CP210xVCPInstaller_x64.exe"
:Win7or8
cd /d %~dp0\drivers\CP210x\Win7or8
start "" "CP210xVCPInstaller_x64.exe"
:Win10
cd /d %~dp0\drivers\CP210x\Win10
start "" "CP210xVCPInstaller_x64.exe"
cd /d %~dp0\drivers\vcp_v1.4.0
start "" "VCP_V1.4.0_Setup.exe"
ping -n 2 127.1>nul
cd /d %~dp0\tools\win\stlink\ST-LINK_USB_V2_1_Driver
start cmd /k call stlink_winusb_install.bat
ping -n 2 127.1>nul
@ echo.

echo  绿化完成，请尽情使用吧
ping -n 2 140.1>nul
goto menu

:HareBootDownload
cd /d %~dp0\HareBootDownload
start cmd /k call 一键引导程序下载.bat
ping -n 2 140.1>nul
goto menu

:Exit
exit
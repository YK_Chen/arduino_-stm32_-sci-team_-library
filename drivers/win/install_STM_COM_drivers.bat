@echo off
>NUL 2>&1 REG.exe query "HKU\S-1-5-19" || (
    ECHO SET UAC = CreateObject^("Shell.Application"^) > "%TEMP%\Getadmin.vbs"
    ECHO UAC.ShellExecute "%~f0", "%1", "", "runas", 1 >> "%TEMP%\Getadmin.vbs"
    "%TEMP%\Getadmin.vbs"
    DEL /f /q "%TEMP%\Getadmin.vbs" 2>NUL
    Exit /b
)
echo Installing STM Serial driver...
"%~dp0wdi-simple" --vid 0x0483 --pid 0x5740 --type 3 --name "STM Serial" --dest "%~dp0maple-serial"
echo.

REM pause
exit


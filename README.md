Arduino STM32
=============

## Notice
This software is experimental and a work in progress.
Under no circumstances should these files be used in relation to any critical system(s).
Use of these files is at your own risk.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

## Summary:
This repo contains the "Hardware" files to support STM32 based boards on Arduino version 1.8.x  and other generic STM32F103 boards.

## Background & Support:
* **Based on the gitee (https://gitee.com/YK_Chen/arduino_-stm32_-sci-team_-library) for full details**


## Purchase info:
### Entry level boards
![](https://img.imgdb.cn/item/600253233ffa7d37b3b51f4d.jpg)  